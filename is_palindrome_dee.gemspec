Gem::Specification.new do |s|
  s.name             = "is_palindrome_dee"
  s.version          = "0.0.3"
  s.summary          = "String Extend"
  s.date             = "2014-02-17"
  s.description      = "The library opens up the String class and checks
  if the string is a palindrome, returning true or false."
  s.authors          = ["Damian Esteban"]
  s.email            = ["damian.esteban@gmail.com"]
  s.homepage         = "http://www.estebanrules.com"
  s.files            = ["lib/is_palindrome_dee.rb"]
end
