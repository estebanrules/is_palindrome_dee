# palindrome?
class String
  def is_palindrome_dee
    self.strip
    self.downcase!
    self.split(' ')
    self == self.reverse
  end
end
